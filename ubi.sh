#!/usr/bin/env bash

set -euo pipefail

BASE_DIR="$( cd "${BASH_SOURCE[0]%/*}" > /dev/null 2>&1 && pwd )"
SCRIPTS_DIR="${BASE_DIR}/scripts"

COMMAND="${1:-help}"

# shellcheck source=scripts/_common.sh
source "${SCRIPTS_DIR}/_common.sh"
# shellcheck source=scripts/prepare.sh
source "${SCRIPTS_DIR}/prepare.sh"
# shellcheck source=scripts/build.sh
source "${SCRIPTS_DIR}/build.sh"
# shellcheck source=scripts/cleanup.sh
source "${SCRIPTS_DIR}/cleanup.sh"
# shellcheck source=scripts/build_release.sh
source "${SCRIPTS_DIR}/build_release.sh"

case "${COMMAND}" in
  prepare)
    prepareImageBuild
    ;;
  build)
    buildImages
    ;;
  cleanup)
    cleanupImageBuild
    ;;
  build_release)
    buildRelease
    ;;
  *)
    echo "Usage: ${0} [prepare|build|cleanup|build_release] [RELEASE_ID] [TARGET_IMAGE_REPOSITORY] [TARGET_PLATFORM(optional)]"
esac
