# GitLab Runner UBI offline build

UBI-based images can be built in an isolated environment with limited access to the Internet.
In such an environment, the build scripts must download the binary dependencies from
[our releases S3 bucket](https://gitlab-runner-downloads.s3.amazonaws.gitlab.com/ubi-images/). They also need
access the official UBI software repositories.

The build process is automated and controlled by the set of scrips, present in this repository.
When used, the scripts will:

1. Download GitLab Runner GPG key from our S3 release bucket.
1. Download required binaries for the given release from our S3 release bucket.
1. Download checksums file and verify integrity and authority of the downloaded binaries using the downloaded GPG key.
1. Build `gitlab/gitlab-runner` and `gitlab/gitlab-runner-helper` images using the provided Dockerfiles, downloaded
   assets and available RedHat repositories for the UBI image.

## How to build

1. Clone the project

    ```bash
    $ git clone https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images.git
    $ cd gitlab-runner-ubi-images
    ```

    or update it, if it's already present locally:

    ```bash
    $ cd gitlab-runner-ubi-images
    $ git checkout master
    $ git pull
    ```

1. Run the `prepare` command to prepare the building workspace

    ```bash
    $ ./ubi.sh prepare
    ```

    By default it will prepare the workspace using the latest `main` binaries. If you want to build for a specific
    release, then use the specific tag, for example:

    ```bash
    $ ./ubi.sh prepare v12.8.0
    ```

    Note that the argument must be equal to the Git tag created in GitLab Runner project, **so it includes the
    `v` at the beginning!**

1. Run the `build` command to build the images

    ```bash
    $ ./ubi.sh build
    ```

    As with prepare, by default the `master` version will be built. Also the `repository` part of the built image
    is by default set to `gitlab` and the command builds these two images:

    - `gitlab/gitlab-runner` - the image with GitLab Runner process inside. This one should be configured
      with the [`image` Helm Chart value](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/v0.13.0/values.yaml#L8).
    - `gitlab/gitlab-runner-helper` - the image used by GitLab Runner's Kubernetes executor to create the _predefined_
      containers in the job pod. The predefined container is used to clone code, download/upload artifacs,
      handle cache extraction and archiving (including the download/upload to the remote cache server if defined). This
      one should be configured with the [`runners.helpers.image` Helm Chart value](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/v0.13.0/values.yaml#L243).

    Both the used release and the image repository can be configured with script parameters. For example to build
    images for the `v12.8.0` release and tag them as `my.own.repository/gitlab-runner-ubi/gitlab-runner`, the following
    command should be used:

    ```bash
    $ /ubi.sh build v12.8.0 my.own.repository/gitlab-runner-ubi
    ```

    This command will additionally create a `ubi-values.yaml` file in the `build/` directory, for example:

    ```yaml
    securityContext:
      runAsUser: 998
      fsGroup: 65534
    image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/test/gitlab-runner:master
    runners:
      helpers:
        image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images/test/gitlab-runner-helper:x86_64-0cd72ffc
    ```

    The file can be next used with [GitLab Runner Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-runner) when
    deploying GitLab Runner, for example:

    ```bash
    $ helm install \
           --values ubi-values.yaml \
           --values user-custom-values.yaml \
           [other options]
           gitlab/gitlab-runner
    ```

1. Run the `cleanup` command to cleanup the workspace and cache directories (optional step)

    ```bash
    $ ./ubi.sh cleanup
    ```

    The script will remove all files downloaded or created locally for the purpose of handling the images building.

## Using different dockerfiles

To use a different dockerfile, add a fourth parameter to the command. If you want to use all defaults but the OpenShift Dockerfile, use the following. This command will build the container with the `OCP.Dockerfile` file.
```bash
$ ./ubi.sh build "" "" "OCP"
```

You can also add in the parameters like so.
```bash
$ ./ubi.sh build v13.2.0 my.own.repository/gitlab-runner-ubi "OCP"
```

## License

MIT
