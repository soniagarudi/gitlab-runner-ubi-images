TINI_VERSION=0.18.0

S3_BUCKET=${S3_BUCKET:-}
S3_PATH=${S3_PATH:-}
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:-}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:-}

_importGPGKey() {
  local KEY_FILE="${1}"
  local KEY_ID="${2}"

  printMessage 1 "Importing GPG key '${KEY_ID}'"

  gpg --import < "${KEY_FILE}"
  gpg --list-keys "${KEY_ID}"
}

_downloadTini() {
  _importGPGKey "${TINI_PUBLIC_GPG_KEY}" "${TINI_PUBLIC_GPG_KEY_ID}"

  curl -Lf "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-static-amd64" -o ./tini-static-amd64
  curl -Lf "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-static-amd64.asc" -o "./tini-static-amd64.asc"

  printMessage 2 "Verifying Tini integrity"

  gpg --verify "./tini-static-amd64.asc"

  mv ./tini-static-amd64 dist/tini
}

_downloadGitLabRunnerWithHelper() {
  _importGPGKey "${GITLAB_RUNNER_PUBLIC_GPG_KEY}" "${GITLAB_RUNNER_PUBLIC_GPG_KEY_ID}"

  printMessage 1 "Downloading GitLab Runner '${RELEASE}' with the helper binary"

  local WORKDIR="./gitlab-runner"
  local RUNNER_BINARY="${WORKDIR}/binaries/gitlab-runner-linux-amd64"
  local HELPER_BINARY="${WORKDIR}/binaries/gitlab-runner-helper/gitlab-runner-helper.x86_64"
  local CHECKSUM_FILE="${WORKDIR}/release.sha256"
  local CHECKSUM_SIGNATURE_FILE="${WORKDIR}/release.sha256.asc"

  mkdir -p "${WORKDIR}/binaries/gitlab-runner-helper"

  curl -Lf "${RUNNER_URL}/binaries/gitlab-runner-linux-amd64" -o "${RUNNER_BINARY}"
  curl -Lf "${RUNNER_URL}/binaries/gitlab-runner-helper/gitlab-runner-helper.x86_64" -o "${HELPER_BINARY}"
  curl -Lf "${RUNNER_URL}/release.sha256" -o "${CHECKSUM_FILE}"
  curl -Lf "${RUNNER_URL}/release.sha256.asc" -o "${CHECKSUM_SIGNATURE_FILE}"

  cd "${WORKDIR}" || return 1

  printMessage 1 "Verifying GitLab Runner binaries integrity"

  gpg --verify release.sha256.asc

  sha256sum --check --strict --ignore-missing release.sha256

  cd .. || return 1

  mv "${RUNNER_BINARY}" dist/gitlab-runner
  mv "${HELPER_BINARY}" dist/gitlab-runner-helper
}

_generateReleaseIndexFile() {
  printMessage 1 "Generating release index file"

  release-index-gen -working-directory dist/ \
                    -project-version "${RELEASE}" \
                    -project-git-ref "${CI_COMMIT_REF_NAME:-HEAD}" \
                    -project-git-revision "${CI_COMMIT_SHA:-HEAD}" \
                    -project-name "GitLab Runner - UBI images dependencies" \
                    -project-repo-url "https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images" \
                    -gpg-key-env GPG_KEY \
                    -gpg-password-env GPG_PASSPHRASE

  # Integrity selfcheck
  cd dist || return 1
  sha256sum --check --strict release.sha256
  cd .. || return 1
  find dist/
}

_uploadReleaseToS3() {
  if [[ -z "${S3_BUCKET}" ]]; then
    echo "Missing S3_BUCKET variable; skipping upload step"
    return 1
  fi

  if [[ -z "${S3_PATH}" ]]; then
    echo "Missing S3_PATH variable; skipping upload step"
    return 1
  fi

  if [[ -z "${AWS_ACCESS_KEY_ID}" ]]; then
    echo "Missing AWS_ACCESS_KEY_ID variable; skipping upload step"
    return 1
  fi

  if [[ -z "${AWS_SECRET_ACCESS_KEY}" ]]; then
    echo "Missing AWS_SECRET_ACCESS_KEY variable; skipping upload step"
    return 1
  fi

  local S3_URL="s3://${S3_BUCKET}/${S3_PATH}/${RELEASE}"

  printMessage 1 "Syncing the release with S3 bucket ${S3_BUCKET}/${S3_PATH}"

  aws s3 sync dist "${S3_URL}" --acl public-read

  printMessage 1 "Check files at ${RELEASE_BASE_URL}/${RELEASE}/index.html"
}

buildRelease() {
  printMessage 0 "Building release"

  local TEMPDIR
  TEMPDIR="$(mktemp -d)"

  mkdir -p "${TEMPDIR}/dist"

  ls -la "${TEMPDIR}"
  cd "${TEMPDIR}" || return 1

  _downloadTini
  _downloadGitLabRunnerWithHelper
  _generateReleaseIndexFile
  _uploadReleaseToS3

  rm -rf "${TEMPDIR}"
}
