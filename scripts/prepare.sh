_initializeWorkspace() {
  printMessage 0 "Initializing workspace"

  mkdir -p "${LOGS_DIR}"
  mkdir -p "${CACHE_DIR}"

  rm -f "${LOGS_DIR}"/*
  rm -rf "${RUNNER_CONTEXT}"
  rm -rf "${HELPER_CONTEXT}"

  cp -a runner/ "${RUNNER_CONTEXT}"
  cp -a helper/ "${HELPER_CONTEXT}"
}

_downloadAndImportGPGKey() {
  printMessage 0 "Initializing GPG Key"

  printMessage 1 "Downloading ${GPG_KEY_URL}"
  curl -Lf "${GPG_KEY_URL}" | gpg --import
  gpg --list-keys "${GPG_KEY_ID}"
}

_downloadBinary() {
  local URL="${1}"
  local CACHE="${CACHE_DIR}/${2}-${RELEASE}"
  local TARGET="${3}"

  printMessage 1 "Downloading ${URL}"

  if [[ ! -f "${CACHE}" ]]; then
    curl -Lf "${URL}" -o "${CACHE}"
  else
    printMessage 2 "Using cached binary from ${CACHE}"
  fi

  cp "${CACHE}" "${TARGET}"
  chmod +x "${TARGET}"
}

_downloadDependencies() {
  printMessage 0 "Downloading dependencies"

  # Cache binaries but always download the checksum file and its signature
  _downloadBinary "${RELEASE_URL}/gitlab-runner" "gitlab-runner" "${RUNNER_BINARY}"
  _downloadBinary "${RELEASE_URL}/gitlab-runner-helper" "gitlab-runner-helper" "${HELPER_BINARY}"
  _downloadBinary "${RELEASE_URL}/tini" "tini" "${TINI_BINARY}"

  printMessage 1 "Downloading ${RELEASE_URL}/release.sha256"
  curl -Lf "${RELEASE_URL}/release.sha256" -o "${CHECKSUM_FILE}"

  printMessage 1 "Downloading ${RELEASE_URL}/release.sha256.asc"
  curl -Lf "${RELEASE_URL}/release.sha256.asc" -o "${CHECKSUM_SIGNATURE_FILE}"
}

_verifyDependenciesIntegrity() {
  printMessage 0 "Verifying dependencies integrity"

  gpg --verify "${CHECKSUM_SIGNATURE_FILE}"

  sha256sum --check --strict --ignore-missing "${CHECKSUM_FILE}"
}

_prepareLicenses() {
  mkdir -p "${LICENSES_DIR}"
  cp LICENSE "${LICENSES_DIR}/"
}

_finalizeWorkspacePreparation() {
  printMessage 0 "Finalizing workspace initialization"

  cp "${RUNNER_BINARY}" "${RUNNER_CONTEXT}"
  cp "${TINI_BINARY}" "${RUNNER_CONTEXT}"
  cp -r "${LICENSES_DIR}" "${RUNNER_CONTEXT}"

  cp "${HELPER_BINARY}" "${HELPER_CONTEXT}"
  cp -r "${LICENSES_DIR}" "${HELPER_CONTEXT}"
}

prepareImageBuild() {
  _initializeWorkspace
  _downloadAndImportGPGKey
  _prepareLicenses
  _downloadDependencies

  # Enter the workspace. From now on everything will be executed
  # in this directory
  cd "${WORKSPACE}" || return 1

  _verifyDependenciesIntegrity
  _finalizeWorkspacePreparation
}
