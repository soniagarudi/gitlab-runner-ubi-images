ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ARG VERSION
LABEL name="GitLab Runner" \
   vendor="GitLab" \
   maintainer="support@gitlab.com" \
   version="${VERSION}" \
   release="${VERSION}" \
   summary="GitLab Runner used to conduct CI/CD jobs on Kubernetes" \
   description="GitLab Runner is used to run CI/CD jobs from GitLab on Kubernetes/OpenShift Platforms. This image will run each CI/CD Build in a separate pod."

ENV HOME /home/gitlab-runner

# Install security updates that might not be included in the base image.
# Without the latest security updates the image can't pass certification.
RUN dnf --disableplugin=subscription-manager -y update-minimal \
    --security --sec-severity=Important --sec-severity=Critical

RUN dnf --disableplugin=subscription-manager install -yb --nodocs \
    ca-certificates \
    openssl \
    procps \
    tzdata

COPY ./tini /usr/local/bin/tini
COPY ./gitlab-runner /usr/local/bin/gitlab-runner
COPY ./entrypoint.sh /entrypoint

RUN chmod +x /usr/local/bin/gitlab-runner && \
    chmod +x /usr/local/bin/tini && \
    chmod +x /entrypoint

COPY ./licenses /licenses

# https://docs.openshift.com/container-platform/4.6/openshift_images/create-images.html#support-arbitrary-user-ids
RUN mkdir -p /etc/gitlab-runner/certs $HOME /secrets && \
    chgrp -R 0 /etc/gitlab-runner && \
    chmod -R g=u /etc/gitlab-runner && \
    chgrp -R 0 $HOME && \
    chmod -R g=u $HOME && \
    chgrp -R 0 /secrets && \
    chmod -R g=u /secrets

USER 1001

STOPSIGNAL SIGQUIT
ENTRYPOINT ["tini", "--", "/entrypoint"]
CMD ["run", "--working-directory=/"]
